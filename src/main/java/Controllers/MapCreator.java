package Controllers;

import Animals.*;

import java.util.*;

import Map.Cell;
import Map.Terrain;
import Map.Time;

public class MapCreator {
    private final Time time;
    private int mapWidth, mapHeight;
    private Random random = new Random();
    public Cell[][] map;
    public HashMap<String, Integer> listOfAmountOfAnimals = new HashMap<>();
    public List<Animal> animalList = new ArrayList<>();
    /**
     * Constructor for the MapCreator class. Initializes a new MapCreator with the specified parameters.
     *
     * @param mapWidth The width of the map.
     * @param mapHeight The height of the map.
     * @param lions The number of lions in the simulation.
     * @param cheetahs The number of cheetahs in the simulation.
     * @param rhinoceros The number of rhinoceros in the simulation.
     * @param zebras The number of zebras in the simulation.
     * @param elephants The number of elephants in the simulation.
     */
    public MapCreator(int mapWidth, int mapHeight, int lions, int cheetahs, int rhinoceros, int zebras, int elephants, Time time) {
        this.mapHeight = mapHeight;
        this.mapWidth = mapWidth;
        listOfAmountOfAnimals.put("Lions", lions);
        listOfAmountOfAnimals.put("Cheetahs", cheetahs);
        listOfAmountOfAnimals.put("Rhinoceros", rhinoceros);
        listOfAmountOfAnimals.put("Zebras", zebras);
        listOfAmountOfAnimals.put("Elephants", elephants);
        map = new Cell[mapWidth][mapHeight];
        this.time = time;
    }
    /**
     * This method is used to create the map of the simulation.
     * It initializes each cell of the map as a new Cell object with the terrain set to GRASS.
     */
    public void createMap() {
        for (int i = 0; i < mapWidth; i++) {
            for (int j = 0; j < mapHeight; j++) {
                map[i][j] = new Cell(false, Terrain.GRASS, i, j);
            }
        }
    }
    /**
     * This method is used to fill the map with animals.
     * It uses the provided HashMap to determine the types and quantities of animals to add to the map.
     *
     * @param animalLists A HashMap where the keys are animal types and the values are the quantities of each animal type.
     */
    public void fillMap(HashMap<String, Integer> animalLists) {

        for (Map.Entry<String, Integer> entry : animalLists.entrySet()) {
            String animalType = entry.getKey();
            int animalAmount = entry.getValue();

            for (int i = 0; i < animalAmount; i++) {
                int x = random.nextInt(mapWidth);
                int y = random.nextInt(mapHeight);

                while (map[x][y].containsAnimal()) {
                    x = random.nextInt(mapWidth);
                    y = random.nextInt(mapHeight);
                }

                Animal animal = createAnimal(animalType, x, y, map);
                if (animal != null) {
                    map[x][y].addAnimal(animal);
                    animalList.add(animal);
                    map[x][y].animalOnCell.time = time;
                }
            }
        }
    }
    /**
     * This method is used to create a new Animal object of a specific type at a specific position on the map.
     *
     * @param animalType The type of animal to create.
     * @param x The X-coordinate of the position on the map.
     * @param y The Y-coordinate of the position on the map.
     * @param map The map of the simulation.
     * @return A new Animal object of the specified type at the specified position on the map.
     */
    private Animal createAnimal(String animalType, int x, int y, Cell[][] map) {
        return switch (animalType) {
            case "Lions" -> new Lion(x, y, map);
            case "Cheetahs" -> new Cheetah(x, y, map);
            case "Rhinoceros" -> new Rhinoceros(x, y, map);
            case "Zebras" -> new Zebra(x, y, map);
            case "Elephants" -> new Elephant(x, y, map);
            default -> null;
        };
    }

}
