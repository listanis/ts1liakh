package test;

import Animals.Animal;
import Animals.Direction;
import Map.Terrain;
import Map.Time;
import Map.Cell;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class AnimalTest {

    private Cell[][] map;
    private Time time;

    @BeforeEach
    public void setUp() {
        int mapWidth = 10;
        int mapHeight = 10;
        map = new Cell[mapWidth][mapHeight];
        for (int i = 0; i < mapWidth; i++) {
            for (int j = 0; j < mapHeight; j++) {
                map[i][j] = mock(Cell.class); // Use Mockito to create a mock Cell
                when(map[i][j].getX()).thenReturn(i);
                when(map[i][j].getY()).thenReturn(j);
            }
        }
        time = mock(Time.class); // Use Mockito to create a mock Time
        when(time.getTime()).thenReturn("real_time");
        Animal.time = time;
        Animal.map = map;
    }

    @Test
    public void testMove() {
        Animal animal = new Animal(1, "TestAnimal", 10, 5, 5, 100, 100, map, time);
        map[5][5].addAnimal(animal);

        animal.move(Direction.UP);
        assertEquals(6, animal.getY());
        assertEquals(5, animal.getX());

        animal.move(Direction.RIGHT);
        assertEquals(6, animal.getY());
        assertEquals(6, animal.getX());

        animal.move(Direction.DOWN);
        assertEquals(5, animal.getY());
        assertEquals(6, animal.getX());

        animal.move(Direction.LEFT);
        assertEquals(5, animal.getY());
        assertEquals(5, animal.getX());
    }

    @Test
    public void testEat() {
        Animal predator = new Animal(1, "Predator", 10, 5, 5, 50, 100, map, time);
        map[5][5].addAnimal(predator);

        Animal prey = new Animal(1, "Prey", 5, 5, 6, 0, 50, map, time);
        map[5][6].addAnimal(prey);

        predator.eat(prey);
        assertEquals(100, predator.getHealth());
        assertFalse(map[5][6].containsAnimal());
    }
    @Test
    public void testAttack() {
        Animal attacker = new Animal(1, "Attacker", 50, 5, 5, 100, 100, map, time);
        map[5][5].addAnimal(attacker);

        Animal target = new Animal(1, "Target", 10, 5, 6, 100, 100, map, time);
        map[5][6].addAnimal(target);

        attacker.attack(target);
        assertEquals(50, target.getHealth()); // Target's health should be reduced by attacker's power

        attacker.attack(target);
        assertEquals(0, target.getHealth()); // Target's health should be reduced again

        // If the target's health becomes zero or less, it should die forever
        attacker.attack(target);
        assertFalse(target.isAlive()); // Target should be dead
        assertEquals(0, target.getHealth()); // Target's health should be zero
    }

    @Test
    public void testDieForever() {
        Animal animal = new Animal(1, "TestAnimal", 10, 5, 5, 100, 100, map, time);
        map[5][5].addAnimal(animal);

        animal.dieForever();

        assertFalse(animal.isAlive()); // Animal should be dead
        assertEquals(0, animal.getHealth()); // Animal's health should be zero
        assertNull(map[5][5].getAnimalOnCell()); // Cell should not contain any animal
    }
}
