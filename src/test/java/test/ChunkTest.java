package test;

import Animals.Animal;
import Animals.Lion;
import Map.Chunk;
import Map.Terrain;
import Map.Time;
import Map.Cell;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

import java.util.List;

public class ChunkTest {

    private Cell[][] map;
    private Time time;

    Chunk chunk = new Chunk(0,0,3,3);


    @BeforeEach
    void setUp() {
        int mapWidth = 10;
        int mapHeight = 10;
        map = new Cell[mapWidth][mapHeight];
        for (int i = 0; i < mapWidth; i++) {
            for (int j = 0; j < mapHeight; j++) {
                map[i][j] = new Cell(false, Terrain.GRASS,i, j); // Assuming Cell constructor takes x and y coordinates
            }
        }
        time = new Time("real_time");
        Animal.time = time;
        }


    @Test
    void testUpdate() {
        Time time = new Time("real_time");
        // Add animals to the chunk
        Lion animal1 = new Lion(2,2, map);
        Lion animal2 = new Lion(9, 9, map);
        map[2][2].addAnimal(animal1);
        map[9][9].addAnimal(animal2);

        chunk.update(); // Update the chunk
        chunk.addAnimal(animal1);
        List<Animal> animals = chunk.getAnimals(); // Get the updated list of animals
        assertEquals(1, animals.size()); // There should be only one animal left
        assertTrue(animals.contains(animal1)); // animal1 should still be in the list
        assertFalse(animals.contains(animal2)); // animal2 should have been removed
    }

    @Test
    void testContains() {
        assertTrue(chunk.contains(1, 1)); // Test with coordinates inside the chunk
        assertFalse(chunk.contains(9, 9)); // Test with coordinates outside the chunk
    }
}
